This test code is running on https://www.tindie.com/products/microwavemont/esp32-simple-alexa/

This code is based on https://github.com/maspetsberger/esp32-i2s-mems/blob/master/examples/VUMeterDemo/VUMeterDemo.ino
which compiles and runs as expected from Arduino ESP32 1.0.2 the major difference is that uses the old `i2s_read_bytes`

When compiled against ESP-IDF `691b81ec2` `i2s_read` produces 128 bytes of zeros.